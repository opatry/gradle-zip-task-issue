Steps to reproduce

- in `gradle/wrapper/gradle-wrapper.properties`, choose `4.10.2`
- launch `./gradlew tasks` that will trigger the custom `proj_zipLogcat` task

Everything is fine on 4.10.2 without any warning.
When switching to 5.0, Gradle complains because of the lack of `destinationDir`.
